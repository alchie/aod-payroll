<style>
<!--
body {
	font-family:sans-serif;
	font-size:12px;
}
h1,h2,h3,h4,h5,h6 {
	margin:0;
	padding:0;
}
.group_title {
	margin-top:10px;
}
/* Employees */
table.employee {
	border:solid 1px #000;
}
table.employee td {
	padding:5px 3px;
	text-align:center;
	font-size:10px;
}
table.employee td a {
	display:block;
	text-decoration:none;
}
table.employee th {
	padding:5px 3px;
	font-size:11px;
	font-weight:bold;
}
.left {
	text-align:left!important;
}
.right {
	text-align:right!important;
}
.bold {
	font-weight:bold;
}
.total-net-pay {
	font-size: 13px!important;
}
a {
	text-decoration:none;
	color:#000;
}
a:hover {
	text-decoration:underline;
}
.credits {
	font-size: 12px;
}
table > thead {
	background: rgba(63,81,181,0.27);
}
table > tfoot {
	background: rgba(33, 150, 243, 0.27);
}
table > tbody.grand-total {
	    background: rgba(233,30,99,0.5);
}
table > tfoot td {
	font-size:12px!important;
}
.daily {
	color: red;
}
.total-item {
	font-weight:bold;
	font-size:11px!important;
}
.net-pay {
	font-size:12px!important;
}
-->
</style>
<h2>The Roman Catholic Bishop of Davao, Inc.</h2>
<h3>PAYROLL SHEET</h3>
<h4>Salary Period: <?php echo date("F d, Y", strtotime($payroll->period_start)); ?> - <?php echo date("F d, Y", strtotime($payroll->period_end)); ?></h4>

<?php $overall = 0; 
foreach( $employee_groups as $group ) { ?>
<h4 class="group_title"><?php echo $group->group_name; ?></h4>
<table border="1" width="100%" cellpadding="0" cellspacing="0" class="employee">
	<thead>
		<tr>
			<th colspan="4">Employee Details</th>
			<th colspan="<?php echo count(gentelella_options($earnings,'id','item_name')) + 1; ?>">EARNINGS</th>
			<th colspan="<?php echo count(gentelella_options($deductions,'id','item_name')) + 1; ?>">DEDUCTIONS</th>
			<th colspan="<?php echo (count(gentelella_options($contributions,'id','item_name'))*2) + 1; ?>">CONTRIBUTIONS</th>
			<th>NET</th>
		</tr>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Position</th>
			<th>Rate/day</th>
			<?php foreach($earnings as $earning) { ?>
				<th><?php echo $earning->item_name; ?></th>
			<?php } ?>
			<th>TOTAL</th>
			<?php foreach($deductions as $deduction) { ?>
				<th><?php echo $deduction->item_name; ?></th>
			<?php } ?>
			<th>TOTAL</th>
			<?php foreach($contributions as $contribution) { ?>
				<th><?php echo $contribution->item_name; ?>-EE</th>
				<th><?php echo $contribution->item_name; ?>-ER</th>
			<?php } ?>
			<th>TOTAL-EE</th>
			<th>PAY</th>
		</tr>
	</thead>
	<tbody>
		<?php $netpay = 0; foreach( $group->members as $num=>$member ) { ?>
		<tr>
			<td width="2%" align="right"><?php echo $num+1; ?></td>
			<td width="150px" class="name left"><a href="<?php echo site_url("employees/update/" . $member->id ); ?>" target="_blank"><?php echo $member->lname; ?>, <?php echo $member->fname; ?> <?php echo $member->mi; ?>.</a></td>
			<td width="100px" class="position"><?php echo $member->position; ?></td>
			<td width="20px" class="daily"><?php echo _nf( $member->daily_rate ); ?></td>
			<?php $total_earnings = 0;
				foreach($earnings as $earning) { 
				$item_id = "item_" . $earning->item_id; 				
				?>
				<td class="<?php echo $item_id; ?>">
				<a href="<?php echo site_url("employees/item_override/" . $member->id . "/" . $earning->item_id) . "?payroll_id=" . $payroll->id; ?>">
				<?php echo _nf( $member->$item_id ); $total_earnings = $total_earnings + intval($member->$item_id); ?>
				</a>
				</td>
			<?php } ?>
			<td class="total-earnings bold total-item"><?php echo _nf($total_earnings); ?></td>
			<?php $total_deductions = 0;
				foreach($deductions as $deduction) { 
				$item_id = "item_" . $deduction->item_id; ?>
				<td width="2%" class="<?php echo $item_id; ?>">
				<a href="<?php echo site_url("employees/item_override/" . $member->id . "?item_id=" . $deduction->item_id . "&payroll_id=" . $payroll->id); ?>">
				<?php echo _nf( $member->$item_id ); $total_deductions = $total_deductions + intval($member->$item_id); ?>
				</a>
				</td>
			<?php } ?>
			<td class="total-deductions bold total-item"><?php echo _nf($total_deductions); ?></td>
			<?php $total_contributions = 0;
				foreach($contributions as $contribution) { 
				$item_id = "item_employee_" . $contribution->item_id;
				$item_id2 = "item_employer_" . $contribution->item_id;
				?>
				<td class="<?php echo $item_id; ?>">
				<a href="<?php echo site_url("employees/item_override/" . $member->id . "?item_id=" . $contribution->item_id . "&payroll_id=" . $payroll->id); ?>">
				<?php echo _nf( $member->$item_id ); $total_contributions = $total_contributions + intval($member->$item_id); ?></a>
				</td>
				<td class="<?php echo $item_id; ?>">
				<?php echo _nf( $member->$item_id2 ); ?>
				</td>
			<?php } ?>
			<td class="total-contributions bold total-item"><?php echo _nf($total_contributions); ?></td>
			<td class="net-pay bold"><?php $netpay1 = ($total_earnings - $total_deductions - $total_contributions); echo _nf($netpay1); $netpay = $netpay + $netpay1; ?></td>
		</tr>
		<?php } ?>
	</tbody>
	
<?php 
$total_values = (object) array();
foreach( $group->members as $member ) { 
		foreach( $member as $key=>$item ) {
			$total_values->$key = (isset($total_values->$key)) ? (intval( $total_values->$key ) + intval($item)) : intval($item); 
		}
}
?>
	<tfoot>
		<tr>
			<td class="total-net-pay right bold" colspan="<?php echo  "4"; //count(gentelella_options($earnings,'id','item_name')) + count(gentelella_options($deductions,'id','item_name')) + (count(gentelella_options($contributions,'id','item_name'))*2) + 7; ?>">TOTAL</td>
			<?php $total_earnings = 0;
				foreach($earnings as $earning) { 
				$item_id = "item_" . $earning->item_id; 				
				?>
				<td class="<?php echo $item_id; ?>">
				<?php echo _nf( $total_values->$item_id ); $total_earnings = $total_earnings + intval($total_values->$item_id); ?>
				</td>
			<?php } ?>
			<td class="total-earnings bold"><?php echo _nf($total_earnings); ?></td>
			<?php $total_deductions = 0;
				foreach($deductions as $deduction) { 
				$item_id = "item_" . $deduction->item_id; ?>
				<td class="<?php echo $item_id; ?>">
				<?php echo _nf( $total_values->$item_id ); $total_deductions = $total_deductions + intval($total_values->$item_id); ?>
				</td>
			<?php } ?>
			<td class="total-deductions bold"><?php echo _nf($total_deductions); ?></td>
			<?php $total_contributions = 0;
				foreach($contributions as $contribution) { 
				$item_id = "item_employee_" . $contribution->item_id;
				$item_id2 = "item_employer_" . $contribution->item_id;
				?>
				<td class="<?php echo $item_id; ?>">
				<?php echo _nf( $total_values->$item_id ); $total_contributions = $total_contributions + intval($total_values->$item_id); ?>
				</td>
				<td class="<?php echo $item_id; ?>">
				<?php echo _nf( $total_values->$item_id2 ); ?>
				</td>
			<?php } ?>
			<td class="total-contributions bold"><?php echo _nf($total_contributions); ?></td>
			<td class="total-net-pay bold"><?php echo _nf( $netpay ); $overall = $overall + $netpay; ?></td>
		</tr>
	</tfoot>
</table>
<?php } ?>
<table border="1" width="100%" cellpadding="0" cellspacing="0" class="employee" style="margin-top:20px">
	<tbody class="grand-total">
		<tr>
			<td class="total-net-pay right bold" colspan="<?php echo count(gentelella_options($earnings,'id','item_name')) + count(gentelella_options($deductions,'id','item_name')) + (count(gentelella_options($contributions,'id','item_name'))*2) + 7; ?>">GRAND TOTAL</td>
			<td class="total-net-pay bold" colspan="<?php echo count(gentelella_options($earnings,'id','item_name')) + count(gentelella_options($deductions,'id','item_name')) + (count(gentelella_options($contributions,'id','item_name'))*2) + 3; ?>"><?php echo _nf( $overall ); ?></td>
		</tr>
	</tbody>
</table>

<table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-top:40px">
	<tbody>
		<tr>
			<td width="33%" class="credits left bold" >Prepared by: <br><br><br><span style="text-transform:uppercase;text-decoration:underline">Chester Alan B. Tagudin</span><br>Bookkeeper / Accountant</td>
			<td width="33%" class="credits left bold" >Checked by: <br><br><br><span style="text-transform:uppercase;text-decoration:underline">Siegfred Alegro</span><br>Bookkeeper</td>
			<td width="33%" class="credits left bold" >Approved by: <br><br><br><span style="text-transform:uppercase;text-decoration:underline">Msgr. Paul A. Cuison</span><br>Oeconomus</td>
		</tr>
	</tbody>
</table>
