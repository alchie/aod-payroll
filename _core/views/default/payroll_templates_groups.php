<?php $this->load->view('header'); $grps = array(); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3><?php echo $template->name; ?> - Employee Groups <a href="<?php echo site_url("payroll_templates"); ?>" class="btn btn-danger btn-xs"><i class="fa fa-arrow-left"></i> Back</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Name </th>
                        <th class="column-title no-link last" width="7%"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $pt_groups as $pt_group ): 
							$grps[] = $pt_group->group_id; ?>
                      <tr class="pointer">
                        <td class=" "><?php echo $pt_group->group_name; ?></td>
                        <td class=" last">
						<a href="<?php echo site_url("payroll_templates/groups/".$template_id."?delete=" . $pt_group->id); ?>">Delete</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>
</table>
 <?php 
 $groups1 = array();
	foreach( $groups as $grp ) {
		if( ! in_array( $grp->id, $grps ) ) {
			$groups1[$grp->id] = $grp->name;
		}
	}
	
	if( count( $groups1 ) > 0 ) {
 ?>
 <?php echo form_open( uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php
	
	$forms = array(
		'group' => array("title"=>"Group", 'type'=>"select_single", "default"=>"", "options"=>$groups1 ),
	);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
?>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" class="btn btn-success">Add <i class="fa fa-plus"></i></button>
                       </div>
                    </div>

</form>
<?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
