 <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="<?php echo site_url("welcome"); ?>" class="site_title"><i class="fa fa-leaf"></i> <span>Payroll System</span></a>
          </div>
          <div class="clearfix"></div>


          <br />
	  <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

<?php $sidebar_menus = array(
	array(	
			'main'=>array('title' => "Payroll Information","icon"=>"fa fa-book","uri"=>"payroll_information"),
			'sub' => array(
				array('title' => "Payroll","uri"=>"payroll"),
				array('title' => "Templates","uri"=>"payroll_templates"),

			) 
	),
	array(	
			'main'=>array('title' => "Employee Information","icon"=>"fa fa-users","uri"=>"employee_information"),
			'sub' => array(
				array('title' => "Employees","uri"=>"employees"),
				array('title' => "Groups","uri"=>"employee_groups"),
			) 
	),
);

if( $this->session->userdata('logged_account_type') == 'admin' ) {
	$sidebar_menus[] = array(	
			'main'=>array('title' => "Administration","icon"=>"fa fa-cogs","uri"=>"administration"),
			'sub' => array(
				array('title' => "User Accounts","uri"=>"user_accounts"),
				array('title' => "Financial Items","uri"=>"financial_items"),
			) 
	);
}
?>
	<?php foreach( $sidebar_menus as $menu ): ?>
            <div class="menu_section">
              <h3><?php echo $menu['main']['title']; ?></h3>
              <ul class="nav side-menu">
                <li class="<?php echo ($menu['main']['uri']==$sidebar_menu_main) ? "active" : ""; ?>"><a><i class="<?php echo $menu['main']['icon']; ?>"></i> <?php echo $menu['main']['title']; ?> <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: <?php echo ($menu['main']['uri']==$sidebar_menu_main) ? "block" : "none"; ?>">
					<?php foreach( $menu['sub'] as $sub ): ?>
                    <li class="<?php echo ($sub['uri']==$sidebar_menu_sub) ? "active current-page" : ""; ?>"><a href="<?php echo site_url($sub['uri']); ?>"><?php echo $sub['title']; ?></a></li>
					<?php endforeach; ?>
                  </ul>
                </li>
                </li>
              </ul>
            </div>
	<?php endforeach; ?>
          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url("welcome/logout"); ?>">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>