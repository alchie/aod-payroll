<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="">
            
			  <h3><?php echo $group->name; ?> <a href="<?php echo site_url("employee_groups"); ?>" class="btn btn-danger btn-xs"><i class="fa fa-arrow-left"></i> Back</a></h3>
			  
          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
<?php if( count( $employees ) > 0 ) { ?>
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Last Name </th>
                        <th class="column-title">First Name </th>
						<th class="column-title">MI</th>
						<th class="column-title">Position </th>
						<th class="column-title">Daily Rate</th>
                        <th class="column-title" width="10%">Status </th>
                        <th class="column-title no-link last" width="10%"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $employees as $emp ): ?>
                      <tr class="pointer <?php echo ($emp->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $emp->lname; ?></td>
                        <td class=" "><?php echo $emp->fname; ?></td>
						<td class=" "><?php echo $emp->mi; ?></td>
						<td class=" "><?php echo $emp->position; ?></td>
						<td class=" "><?php echo $emp->daily_rate; ?></td>
                        <td class=" "><?php echo ($emp->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last"><a href="<?php echo site_url("employees/update/" . $emp->id ); ?>">Update</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
<?php } else { echo "No Employees Found!"; }?>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
