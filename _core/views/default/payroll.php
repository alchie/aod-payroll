<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Payroll <a href="<?php echo site_url("payroll/add"); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Payroll</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Month </th>
						<th class="column-title">Year </th>
                        <th class="column-title">Period</th>
						<th class="column-title">Working Days</th>
                        <th class="column-title">Template</th>
                        <th class="column-title no-link last" width="13%"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $payrolls as $payroll ): ?>
                      <tr class="pointer <?php echo ($payroll->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo date("F", strtotime( $payroll->month . "/1/00")); ?></td>
                        <td class=" "><?php echo $payroll->year; ?></td>
                        <td class=" "><?php echo date("F d, Y", strtotime($payroll->period_start)); ?> - <?php echo date("F d, Y", strtotime($payroll->period_end)); ?></td>
						<td class=" "><?php echo $payroll->days; ?></td>
						<td class=" "><?php echo $payroll->template_name; ?></td>
                        <td class=" last">
							<a class="btn btn-default btn-xs" href="<?php echo site_url("payroll/update/" . $payroll->id); ?>">Update</a>
							<a class="btn btn-default btn-xs" target="_blank" href="<?php echo site_url("payroll/pr1nt/" . $payroll->id); ?>">Print</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
