<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Update Payroll</h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
				  <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open( uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php
	$months = array(
		"01" => "January",
		"02" => "February",
		"03" => "March",
		"04" => "April",
		"05" => "May",
		"06" => "June",
		"07" => "July",
		"08" => "August",
		"09" => "September",
		"10" => "October",
		"11" => "November",
		"12" => "December",
	);
	$temp = array();
	foreach( $templates as $tmp ) {
		$temp[$tmp->id] = $tmp->name;
	}
	$forms = array(
		'month' => array("title"=>"Month", 'type'=>"select_single", "attributes"=>array("required"=>"required"), "options"=>$months, "default"=>$payroll->month),
		'year' => array("title"=>"Year", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$payroll->year),
		'desc' => array("title"=>"Description", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$payroll->description),
		'period_start' => array("title"=>"Period Start", 'type'=>"datepicker", "attributes"=>array("required"=>"required"), "default"=>$payroll->period_start),
		'period_end' => array("title"=>"Period End", 'type'=>"datepicker", "attributes"=>array("required"=>"required"), "default"=>$payroll->period_end),
		'days' => array("title"=>"Number of Days", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$payroll->days),
		'template' => array("title"=>"Template", 'type'=>"select_single", "attributes"=>array("required"=>"required"), "options"=>$temp, "default"=>$payroll->template),
		'status' => array("title"=>"Status", 'type'=>"checkbox", 'options'=>array("1"=>"Active"), "default"=>$payroll->active),
	);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
	?>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<a href="<?php echo site_url("payroll"); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                      </div>
                    </div>

                  </form>
                </div>
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
