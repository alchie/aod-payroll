<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Payroll Templates <a href="<?php echo site_url("payroll_templates/add"); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Template</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Name </th>
                        <th class="column-title">Status </th>
                        <th class="column-title no-link last" width="47%"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $temps as $temp ): ?>
                      <tr class="pointer <?php echo ($temp->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $temp->name; ?></td>
                        <td class=" "><?php echo ($temp->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last">
						<a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/update/" . $temp->id); ?>">Update</a>
						 <a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/groups/" . $temp->id); ?>">Employee Groups</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/earnings/" . $temp->id); ?>">Earnings</a>
						 <a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/deductions/" . $temp->id); ?>">Deductions</a>
						 <a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/contributions/" . $temp->id); ?>">Contributions</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
