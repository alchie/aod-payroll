<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="">
           
              
			  <h3>Add Earning : <?php echo $employee->lname . ", " . $employee->fname; ?> - <?php echo $item->name; ?></h3>
			  
          

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
				  <div class="x_content">
				  <?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>
                  <br />
                  <?php echo form_open( uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
				  
<?php

	$forms = array(
		//'item_id' => array("title"=>"Earning", 'type'=>"select_single", "attributes"=>array("required"=>"required"), "options"=>gentelella_options($earnings, 'id', 'name'), "default"=>(($this->input->get('item_id')!="")? $this->input->get('item_id') : "")),
		'amount' => array("title"=>"Amount", 'type'=>"text", "attributes"=>array("required"=>"required"), "default"=>$data->amount),
	);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
	?>
	
                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">Submit <i class="fa fa-arrow-right"></i></button>
						<?php if( $this->input->get("payroll_id") ) { ?>
						<input type="hidden" name="payroll_id" value="<?php echo $this->input->get("payroll_id"); ?>">
						<a href="<?php echo site_url("payroll/pr1nt/" . $this->input->get("payroll_id")); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
						<?php } else { ?>
						<a href="<?php echo site_url("employees/earnings/" . $employee->id); ?>" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
						<?php } ?>
                      </div>
                    </div>

                  </form>
                </div>
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
