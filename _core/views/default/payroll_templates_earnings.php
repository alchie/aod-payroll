<?php $this->load->view('header'); $grps = array(); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3><?php echo $template->name; ?> - Earnings <a href="<?php echo site_url("payroll_templates"); ?>" class="btn btn-danger btn-xs"><i class="fa fa-arrow-left"></i> Back</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Item </th>
						<th class="column-title">Priority</th>
                        <th class="column-title no-link last" width="7%"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $pt_items as $pt_item ): 
							$grps[] = $pt_item->item_id; ?>
                      <tr class="pointer">
                        <td class=" "><?php echo $pt_item->item_name; ?></td>
						<td class=" "><?php echo $pt_item->priority; ?></td>
                        <td class=" last">
						<a class="btn btn-default btn-xs" href="<?php echo site_url("payroll_templates/earnings/".$template_id."?delete=" . $pt_item->id); ?>">Delete</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>
</table>
<?php 
				  if( validation_errors() ) {
					echo "<div class=\"alert alert-danger\">";
					echo validation_errors(); 
					echo "</div>";
				  }
				  ?>

 <?php 
 $fin_items = array();
	foreach( $earnings as $ear ) {
		if( ! in_array( $ear->item_id, $grps ) ) {
			$fin_items[$ear->item_id] = $ear->name;
		}
	}
	if( count( $fin_items ) > 0 ) {
 ?>
 <?php echo form_open( uri_string(), array("id"=>"","class"=>"form-horizontal form-label-left")); ?>
<?php
	
	$forms = array(
		'item_id' => array("title"=>"Item", 'type'=>"select_single", "default"=>"", "options"=>$fin_items ),
		'priority' => array("title"=>"Priority", 'type'=>"text", "default"=>"1" ),
	);
	
	foreach($forms as $key=>$form ) {
		echo gentelella_form1( $form['type'], $form['title'], $key, $form, $form['default'] ); 
	}
?>

                    <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="submit" class="btn btn-success">Add <i class="fa fa-plus"></i></button>
                       </div>
                    </div>

</form>
<?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
