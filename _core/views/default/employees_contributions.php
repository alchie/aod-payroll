<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="">
           
              
			  <h3>Employee Contributions : <?php echo $employee->lname . ", " . $employee->fname; ?> <a href="<?php echo site_url("employees/contributions_add/" . $employee->id); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Contribution</a></h3>
			  
          

          </div>
          <div class="clearfix"></div>

           <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Item</th>
						<th class="column-title">Employee Share</th>
						<th class="column-title">Employer Share</th>
                        <th class="column-title" width="10%">Status </th>
                        <th class="column-title no-link last" width="7%"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $contributions as $deduc ): ?>
                      <tr class="pointer <?php echo ($deduc->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $deduc->item_name; ?></td>
						<td class=" "><?php echo _nf( $deduc->employee ); ?></td>
						<td class=" "><?php echo _nf( $deduc->employer ); ?></td>
                        <td class=" "><?php echo ($deduc->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last">
						<a href="<?php echo site_url("employees/contributions_delete/" . $deduc->employee_id . "?id=" . $deduc->id ); ?>">Delete</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
      
        </div>

<?php $this->load->view('footer'); ?>
