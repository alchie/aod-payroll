<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Financial Items <a href="<?php echo site_url("financial_items/add"); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Item</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Name </th>
						<th class="column-title">Active</th>
                        <th class="column-title no-link last" width="14%"><span class="nobr">Action</span></th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $items as $item ): ?>
                      <tr class="pointer <?php echo ($item->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $item->name; ?></td>
                        <td class=" "><?php echo ($item->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last">
						<a class="btn btn-default btn-xs" href="<?php echo site_url("financial_items/update/" . $item->id); ?>">Update</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("financial_items/delete/" . $item->id); ?>">Delete</a>
						</td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
