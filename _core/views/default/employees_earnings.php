<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="">
           
              
			  <h3>Employee Earnings : <?php echo $employee->lname . ", " . $employee->fname; ?> <!--<a href="<?php echo site_url("employees/earnings_add/" . $employee->id); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Earning</a>--></h3>
			  
          

          </div>
          <div class="clearfix"></div>

           <div class="row">
<!--
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Item</th>
						<th class="column-title">Amount</th>
                        <th class="column-title" width="10%">Status </th>
                        <th class="column-title no-link last" width="12%"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $earnings as $earn ): ?>
                      <tr class="pointer <?php echo ($earn->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $earn->item_name; ?></td>
						<td class=" "><?php echo _nf( $earn->amount ); ?></td>
                        <td class=" "><?php echo ($earn->active==1) ? "Active" : "Disabled"; ?></td>
                        <td class=" last">
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/earnings_edit/" . $earn->employee_id . "?id=" . $earn->id ); ?>">Edit</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/earnings_delete/" . $earn->employee_id . "?id=" . $earn->id ); ?>">Delete</a>
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
-->

<?php foreach( $items as $item ) { 
if( $item->one_time != 1 ) { 
?>
<a href="<?php echo site_url("employees/earnings_add/" . $employee->id . "/" . $item->item_id ); ?>">
<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="tile-stats">
	<div class="icon"><i class="fa fa-plus"></i>
	</div>
	<div class="count"><?php echo ($item->amount) ? _nf($item->amount) : 0; ?></div>

	<h3><?php echo $item->name; ?></h3>
  </div>
</div>
</a>
<?php }
 } ?>
            </div>
          </div>
      
        </div>

<?php $this->load->view('footer'); ?>
