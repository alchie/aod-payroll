<?php $this->load->view('header'); ?>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

     <?php $this->load->view('sidebar_nav'); ?>

	   <?php $this->load->view('top_nav'); ?>

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="title_left">
              
			  <h3>Employees <a href="<?php echo site_url("employees/add"); ?>" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Employee</a></h3>
			  
            </div>

          </div>
          <div class="clearfix"></div>

          <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <div class="clearfix"></div>
				  
<table class="table table-striped responsive-utilities jambo_table bulk_action">
                    <thead>
                      <tr class="headings">
                        <th class="column-title">Last Name </th>
                        <th class="column-title">First Name </th>
						<th class="column-title">MI</th>
						<!--<th class="column-title">Position </th>
						<th class="column-title">Daily Rate</th>
                        <th class="column-title" width="10%">Status </th>-->
                        <th class="column-title no-link last" width="50%"><span class="nobr">Action</span>
                        </th>
                      </tr>
                    </thead>

                    <tbody>
					<?php foreach( $employees as $emp ): ?>
                      <tr class="pointer <?php echo ($emp->active==1) ? "" : "danger"; ?>">
                        <td class=" "><?php echo $emp->lname; ?></td>
                        <td class=" "><?php echo $emp->fname; ?></td>
						<td class=" "><?php echo $emp->mi; ?></td>
						<!--<td class=" "><?php echo $emp->position; ?></td>
						<td class=" "><?php echo $emp->daily_rate; ?></td>
                        <td class=" "><?php echo ($emp->active==1) ? "Active" : "Disabled"; ?></td>-->
                        <td class=" last">
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/update/" . $emp->id ); ?>">Update</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/earnings/" . $emp->id ); ?>">Earnings</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/deductions/" . $emp->id ); ?>">Deductions</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/contributions/" . $emp->id ); ?>">Contributions</a>
						<a class="btn btn-default btn-xs" href="<?php echo site_url("employees/cash_advances/" . $emp->id ); ?>">Cash Advances</a>
						
                        </td>
                      </tr>
					<?php endforeach; ?>
					</tbody>

                  </table>
                
				  
				  
                </div>
              </div>
            </div>
          </div>
        </div>

<?php $this->load->view('footer'); ?>
