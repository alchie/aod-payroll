<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_account_type') != 'admin' ) {
			redirect("welcome", "401");
		}
		
		$this->data['sidebar_menu_main'] = "employee_information";
		$this->data['sidebar_menu_sub'] = "employees";
		$this->load->helper("gentelella");
		
		$this->load->model(array(
			'Employees_model',
			'Employee_earnings_model',
			'Employee_deductions_model',
			'Employee_contributions_model',
			'Employee_groups_model',
			'Employee_items_model',
			));
	}
	
	public function index()
	{
		$this->load->model('Employees_model');
		$emps = new Employees_model();
		$emps->setLimit(0);
		$this->data['employees'] = $emps->populate();
		$this->load->view('employees', $this->data );
	}
		
	public function add() {
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('mi', 'Middle Initial', 'trim|required');
			$this->form_validation->set_rules('position', 'Position', 'trim|required');
			$this->form_validation->set_rules('daily_rate', 'Daily Rate', 'trim|required|decimal');
			
			 if ($this->form_validation->run() == TRUE) {
			 
				$lname = $this->input->post("lname", true);
				$fname = $this->input->post("fname", true);
				$mi = $this->input->post("mi", true);
				$position = $this->input->post("position", true);
				$daily_rate = $this->input->post("daily_rate", true);
				$group = $this->input->post("group", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$emp = new $this->Employees_model();
				$emp->setLname( $lname );
				$emp->setFname( $fname );
				$emp->setMi( $mi );
				$emp->setPosition( $position );
				$emp->setDailyRate( $daily_rate );
				$emp->setGroup( $group );
				$emp->setActive( "1" );
				if( $emp->insert() ) {
					redirect("employees/update/" . $emp->getId(), "location");
				}
			}
		}
		
		$empgroup = new $this->Employee_groups_model;
		$this->data['groups'] = $empgroup->populate();
		
		$this->load->view('employees_add', $this->data);
	}
	
	public function update($id) {
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('mi', 'Middle Initial', 'trim|required');
			$this->form_validation->set_rules('position', 'Position', 'trim|required');
			$this->form_validation->set_rules('daily_rate', 'Daily Rate', 'trim|required|decimal');
			
			 if ($this->form_validation->run() == TRUE) {
			 
				$lname = $this->input->post("lname", true);
				$fname = $this->input->post("fname", true);
				$mi = $this->input->post("mi", true);
				$position = $this->input->post("position", true);
				$daily_rate = $this->input->post("daily_rate", true);
				$group = $this->input->post("group", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$emp->setLname( $lname );
				$emp->setFname( $fname );
				$emp->setMi( $mi );
				$emp->setPosition( $position );
				$emp->setDailyRate( $daily_rate );
				$emp->setGroup( $group );
				$emp->setActive( $status );
				$emp->update();
			}
		}
		
		$this->data['employee'] = $emp->get();
		
		$empgroup = new $this->Employee_groups_model;
		$this->data['groups'] = $empgroup->populate();
		
		$this->load->view('employees_update', $this->data);
	}
	
	public function earnings($id) {
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$earnings = new $this->Employee_earnings_model();
		$earnings->setEmployeeId( $id, true );
		$earnings->setSelect("employee_earnings.*, financial_items.name as item_name");
		$earnings->setJoin("financial_items", "financial_items.id = employee_earnings.item_id");
		$this->data['earnings'] = $earnings->populate();
		
		$this->load->model('Financial_items_settings_model');
		$items = new $this->Financial_items_settings_model();
		$items->setKey('type', true);
		$items->setValue('earning', true);
		$items->setJoin('financial_items', 'financial_items.id = financial_items_settings.item_id');
		$items->setSelect("financial_items.*");
		$items->setSelect("financial_items_settings.*");
		$items->setSelect("(SELECT amount FROM employee_items ei WHERE ei.employee_id='{$id}' AND ei.item_id=financial_items.id LIMIT 1) as amount");
		$items->setSelect("(SELECT value FROM financial_items_settings fi WHERE fi.item_id=financial_items.id AND fi.key='principal' LIMIT 1) as principal");
		$items->setSelect("(SELECT value FROM financial_items_settings fi WHERE fi.item_id=financial_items.id AND fi.key='one_time' LIMIT 1) as one_time");
		$items->setSelect("(SELECT value FROM financial_items_settings fi WHERE fi.item_id=financial_items.id AND fi.key='repeat' LIMIT 1) as repeat1");
		$this->data['items'] = $items->populate();
		
		$this->load->view('employees_earnings', $this->data);
	}
	
	public function earnings_add( $id, $item_id ) {
		
		$this->load->model('Financial_items_settings_model');
		$set = new $this->Financial_items_settings_model;
		$set->setItemId($item_id, true);
		
		$fi_settings = array();
		foreach( $set->populate() as $fis ) {
			$fi_settings[$fis->key] = $fis->value;
		}
		$this->data['settings'] = (object) $fi_settings;
		
		$emp = new $this->Employee_items_model();
		$emp->setEmployeeId( $id, true, false );
		$emp->setItemId( $item_id, true, false );
		$emp->setOrder( 'id', 'DESC' );
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {

				$amount = $this->input->post("amount", true);
				
				//$emp = new $this->Employee_items_model();
				$emp->setAmount( $amount, false, true );
				$emp->setAmount2( "0" );
				$emp->setActive( "1" );
				
				if( isset($this->data['settings']->repeat) && ($this->data['settings']->repeat == 1 ) ) { 
					if( $emp->nonEmpty() === TRUE ) {
						$emp->setId( $emp->getResults()->id, true, false );
						$emp->update();
					}
				} elseif( isset($this->data['settings']->principal) && ($this->data['settings']->principal == 1 ) ) {
					$emp->setEmployeeId( $id, false, false );
					$emp->setItemId( $item_id, false, false );
					$emp->insert();
				}
				if( $this->input->post("payroll_id") ) {
					redirect("payroll/pr1nt/" . $this->input->post("payroll_id"), "location");
				} else {
					redirect("employees/earnings/" . $id , "location");
				}
			}
		}
		if( isset($this->data['settings']->repeat) && ($this->data['settings']->repeat == 1 ) ) { 
			$this->data['data'] = $emp->get();
		} elseif( isset($this->data['settings']->principal) && ($this->data['settings']->principal == 1 ) ) { 
			$this->data['data'] = $emp->populate();
		}
		
		$emp1 = new $this->Employees_model();
		$emp1->setId( $id, true );
		$this->data['employee'] = $emp1->get();

		$this->load->model('Financial_items_model');
		$fi = new $this->Financial_items_model;
		$fi->setId( $item_id, true );
		
		$this->data['item'] = $fi->get();
		
		$this->load->view('employees_earnings_add', $this->data);
	}
	
	public function earnings_edit($id) {
		
		$empE = new $this->Employee_earnings_model();
		$empE->setId( $this->input->get("id"), true, false );
		$empE->setEmployeeId( $id, true, false );
		
		if( count($this->input->post()) > 0 ) {
			//$this->form_validation->set_rules('item_id', 'Earning', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			 
				//$item_id = $this->input->post("item_id", true);
				$amount = $this->input->post("amount", true);
				$active = $this->input->post("status", true);
				
				//$emp->setItemId( $item_id );
				$empE->setAmount( $amount, false, true );
				$empE->setActive( $active, false, true );
				
				if( $empE->update() ) {
					redirect("employees/earnings/" . $id, "location");
					exit;
				}
			}
		}
		
		$this->data['employee_earning'] = $empE->get();
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
				
		$this->load->view('employees_earnings_edit', $this->data);
	}
	
	public function earnings_delete($id) {
	
		$emp = new $this->Employee_earnings_model();
		$emp->setId( $this->input->get('id'), true);
		$emp->delete();
		redirect("employees/earnings/" . $id, "location");
		exit;
	}
	
	public function deductions($id) {
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$earnings = new $this->Employee_deductions_model();
		$earnings->setEmployeeId( $id, true );
		$earnings->setSelect("employee_deductions.*, financial_items.name as item_name");
		$earnings->setJoin("financial_items", "financial_items.id = employee_deductions.item_id");
		$this->data['deductions'] = $earnings->populate();
		
		$this->load->view('employees_deductions', $this->data);
	}
	
	public function deductions_add($id) {
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('item_id', 'Deduction', 'trim|required');
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			 
				$item_id = $this->input->post("item_id", true);
				$amount = $this->input->post("amount", true);
				
				$emp = new $this->Employee_deductions_model();
				$emp->setEmployeeId( $id );
				$emp->setItemId( $item_id );
				$emp->setAmount( $amount );
				$emp->setActive( "1" );
				if( $emp->insert() ) {
					redirect("employees/deductions/" . $id, "location");
					exit;
				}
			}
		}
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();

		$this->load->model('Financial_items_settings_model');
		$items = new $this->Financial_items_settings_model();
		$items->setKey('type', true);
		$items->setValue('deduction', true);
		$items->setJoin('financial_items', 'financial_items.id = financial_items_settings.item_id');
		$this->data['deductions'] = $items->populate();
		
		$this->load->view('employees_deductions_add', $this->data);
	}
	
	public function deductions_edit($id) {
		
		$emp = new $this->Employee_deductions_model();
		$emp->setEmployeeId( $id, true, false );
		$emp->setId( $this->input->get("id"), true, false );
		
		if( count($this->input->post()) > 0 ) {
			
			$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			 
				$amount = $this->input->post("amount", true);
				$status = $this->input->post("status", true);
				
				$emp->setAmount( $amount, false, true );
				$emp->setActive( $status, false, true );
				if( $emp->update() ) {
					//redirect("employees/deductions/" . $id, "location");
					//exit;
				}
			}
		}
		$this->data['employee_deduction'] = $emp->get();
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
				
		$this->load->view('employees_deductions_edit', $this->data);
	}
	
	public function deductions_delete($id) {
	
		$emp = new $this->Employee_deductions_model();
		$emp->setId( $this->input->get('id'), true);
		$emp->delete();
		redirect("employees/deductions/" . $id, "location");
		exit;
	}
	
	public function contributions($id) {
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$earnings = new $this->Employee_contributions_model();
		$earnings->setEmployeeId( $id, true );
		$earnings->setSelect("employee_contributions.*, financial_items.name as item_name");
		$earnings->setJoin("financial_items", "financial_items.id = employee_contributions.item_id");
		$this->data['contributions'] = $earnings->populate();
		
		$this->load->view('employees_contributions', $this->data);
	}
	
	public function contributions_add($id) {
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('item_id', 'Earning', 'trim|required');
			$this->form_validation->set_rules('employee_share', 'Employee Share', 'trim|required');
			$this->form_validation->set_rules('employer_share', 'Employer Share', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			 
				$item_id = $this->input->post("item_id", true);
				$employee_share = $this->input->post("employee_share", true);
				$employer_share = $this->input->post("employer_share", true);
				
				$emp = new $this->Employee_contributions_model();
				$emp->setEmployeeId( $id );
				$emp->setItemId( $item_id );
				$emp->setEmployee( $employee_share );
				$emp->setEmployer( $employer_share );
				$emp->setActive( "1" );
				if( $emp->insert() ) {
					redirect("employees/contributions/" . $id, "location");
					exit;
				}
			}
		}
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$this->load->model('Financial_items_model');
		$items = new $this->Financial_items_model();
		$items->setType('contribution', true);
		$this->data['contributions'] = $items->populate();
		
		$this->load->view('employees_contributions_add', $this->data);
	}
	
	public function contributions_delete($id) {
	
		$emp = new $this->Employee_contributions_model();
		$emp->setId( $this->input->get('id'), true);
		$emp->delete();
		redirect("employees/contributions/" . $id, "location");
		exit;
	}
	
	public function cash_advances($id) {
		
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$earnings = new $this->Employee_cash_advances_model();
		$earnings->setEmployeeId( $id, true );
		$earnings->setSelect("employee_cash_advances.*, financial_items.name as item_name");
		$earnings->setJoin("financial_items", "financial_items.id = employee_cash_advances.item_id");
		$this->data['cash_advances'] = $earnings->populate();
		
		$this->load->view('employees_cash_advances', $this->data);
	}
	
	public function cash_advances_add($id) {
		
		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('item_id', 'Earning', 'trim|required');
			$this->form_validation->set_rules('employee_share', 'Employee Share', 'trim|required');
			$this->form_validation->set_rules('employer_share', 'Employer Share', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			 
				$item_id = $this->input->post("item_id", true);
				$employee_share = $this->input->post("employee_share", true);
				$employer_share = $this->input->post("employer_share", true);
				
				$emp = new $this->Employee_cash_advances_model();
				$emp->setEmployeeId( $id );
				$emp->setItemId( $item_id );
				$emp->setEmployee( $employee_share );
				$emp->setEmployer( $employer_share );
				$emp->setActive( "1" );
				if( $emp->insert() ) {
					redirect("employees/cash_advances/" . $id, "location");
					exit;
				}
			}
		}
		$emp = new $this->Employees_model();
		$emp->setId( $id, true );
		
		$this->data['employee'] = $emp->get();
		
		$this->load->model('Financial_items_model');
		$items = new $this->Financial_items_model();
		$items->setType('contribution', true);
		$this->data['cash_advances'] = $items->populate();
		
		$this->load->view('employees_cash_advances_add', $this->data);
	}
	
	public function cash_advances_delete($id) {
	
		$emp = new $this->Employee_cash_advances_model();
		$emp->setId( $this->input->get('id'), true);
		$emp->delete();
		redirect("employees/cash_advances/" . $id, "location");
		exit;
	}
}
