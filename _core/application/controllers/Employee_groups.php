<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_groups extends CI_Controller {

	
	
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_account_type') != 'admin' ) {
			redirect("welcome", "401");
		}
		
		$this->data['sidebar_menu_main'] = "employee_information";
		$this->data['sidebar_menu_sub'] = "employee_groups";
		$this->load->helper("gentelella");
		
		$this->load->model('Employee_groups_model');
		
	}
	
	public function index()
	{
		$results = new $this->Employee_groups_model();
		$this->data['groups'] = $results->populate();
		$this->load->view('employee_groups', $this->data );
	}
	
	public function employees( $id )
	{
		$group = new $this->Employee_groups_model();
		$group->setId( $id, true );
		$this->data['group'] = $group->get();
		
		$this->load->model('Employees_model');
		$emps = new Employees_model();
		$emps->setGroup( $id, true );
		$this->data['employees'] = $emps->populate();
		$this->load->view('employee_groups_members', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Template Name', 'trim|required|min_length[5]|is_unique[employee_groups.name]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$temp = new $this->Employee_groups_model();
				$temp->setName( $name );
				$temp->setActive( $status );
				if( $temp->insert() ) {
					redirect("employee_groups/update/" . $temp->getId(), "location");
				}
			}
		}
		
		$this->load->view('employee_groups_add', $this->data);
	}
	
	public function update($id) {
		
		$temp = new $this->Employee_groups_model();
		$temp->setId( $id, true );
		
		$template = $temp->get();
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$name = $this->input->post("name", true);
				$status = $this->input->post("status", true);
				if( $status == "") {
					$status = 0;
				}
				$temp->setName( $name );
				$temp->setActive( $status );

				$temp->update();
			}
		}
		
		$this->data['group'] = $temp->get();
		$this->load->view('employee_groups_update', $this->data);
	}
}
