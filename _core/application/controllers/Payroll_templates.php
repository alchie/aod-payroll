<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_templates extends CI_Controller {

	
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_account_type') != 'admin' ) {
			redirect("welcome", "401");
		}
		
		$this->data['sidebar_menu_main'] = "payroll_information";
		$this->data['sidebar_menu_sub'] = "payroll_templates";
		$this->load->helper("gentelella");
		
		$this->load->model('Payroll_templates_model');
		
	}
	
	public function index()
	{
		$temps = new $this->Payroll_templates_model();
		$this->data['temps'] = $temps->populate();
		$this->load->view('payroll_templates', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Template Name', 'trim|required|min_length[5]|is_unique[payroll_templates.name]');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$temp = new $this->Payroll_templates_model();
				$temp->setName( $name );
				$temp->setActive( $status );
				if( $temp->insert() ) {
					redirect("payroll_templates/update/" . $temp->getId(), "location");
				}
			}
		}
		
		$this->load->view('payroll_templates_add', $this->data);
	}
	
	public function update($id) {
		
		$temp = new $this->Payroll_templates_model();
		$temp->setId( $id, true );
		
		$template = $temp->get();
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Name', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$name = $this->input->post("name", true);
				$status = $this->input->post("status", true);
				if( $status == "") {
					$status = 0;
				}
				$temp->setName( $name );
				$temp->setActive( $status );

				$temp->update();
			}
		}
		
		$this->data['template'] = $temp->get();
		$this->load->view('payroll_templates_update', $this->data);
	}
	
	public function groups( $id ) {
		$this->data['template_id'] = $id;
		$this->load->model(array('Employee_groups_model','Payroll_templates_groups_model'));
		
		if( $this->input->get("delete") != "" ) {
			$ptg = new $this->Payroll_templates_groups_model();
			$ptg->setId( $this->input->get("delete"), true );
			$ptg->delete();
			redirect(uri_string(),"location");
		}
		
		$ptGroups = new $this->Payroll_templates_groups_model;
		$ptGroups->setTempId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('group', 'Group', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$grp = $this->input->post("group", true);
				$ptGroups->setGroupId( $grp );
				$ptGroups->insert();
			}
		}
		
		$ptGroups->setSelect("payroll_templates_groups.*, employee_groups.name as group_name");
		$ptGroups->setJoin("employee_groups", "employee_groups.id = payroll_templates_groups.group_id");
		$this->data['pt_groups'] = $ptGroups->populate();
		
		$temp = new $this->Payroll_templates_model();
		$temp->setId( $id, true );
		$this->data['template'] = $temp->get();
		
		$groups = new $this->Employee_groups_model();
		$this->data['groups'] = $groups->populate();
		$this->load->view('payroll_templates_groups', $this->data );
	}
	
	public function earnings( $id ) {
		$this->data['template_id'] = $id;
		$this->load->model(array('Financial_items_model','Payroll_templates_items_model'));
		
		if( $this->input->get("delete") != "" ) {
			$ptg = new $this->Payroll_templates_items_model();
			$ptg->setId( $this->input->get("delete"), true );
			$ptg->delete();
			redirect(uri_string(),"location");
		}
		
		$ptItem = new $this->Payroll_templates_items_model;
		$ptItem->setTempId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('item_id', 'Item', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$item_id = $this->input->post("item_id", true);
				$priority = $this->input->post("priority", true);
				
				$ptItem->setItemId( $item_id );
				$ptItem->setType("earning");
				$ptItem->setPriority($priority);
				$ptItem->insert();
			}
		}
		
		$ptItem->setType("earning", true);
		$ptItem->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$ptItem->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$ptItem->setOrder('payroll_templates_items.priority','ASC');
		$this->data['pt_items'] = $ptItem->populate();

		$temp = new $this->Payroll_templates_model();
		$temp->setId( $id, true );
		$this->data['template'] = $temp->get();
		
		$this->load->model('Financial_items_settings_model');
		$items = new $this->Financial_items_settings_model();
		$items->setKey('type', true);
		$items->setValue('earning', true);
		$items->setJoin('financial_items', 'financial_items.id = financial_items_settings.item_id');
		$items->setSelect("financial_items.*");
		$items->setSelect("financial_items_settings.*");
		$this->data['earnings'] = $items->populate();
		$this->load->view('payroll_templates_earnings', $this->data );
	}
	
	public function deductions( $id ) {
		$this->data['template_id'] = $id;
		$this->load->model(array('Financial_items_model','Payroll_templates_items_model'));
		
		if( $this->input->get("delete") != "" ) {
			$ptg = new $this->Payroll_templates_items_model();
			$ptg->setId( $this->input->get("delete"), true );
			$ptg->delete();
			redirect(uri_string(),"location");
		}
		
		$ptItem = new $this->Payroll_templates_items_model;
		$ptItem->setTempId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('item_id', 'Item', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$item_id = $this->input->post("item_id", true);
				$priority = $this->input->post("priority", true);
				
				$ptItem->setItemId( $item_id );
				$ptItem->setType("deduction");
				$ptItem->setPriority($priority);
				$ptItem->insert();
			}
		}
		
		$ptItem->setType("deduction", true);
		$ptItem->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$ptItem->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$ptItem->setOrder('payroll_templates_items.priority','ASC');
		$this->data['pt_items'] = $ptItem->populate();

		$temp = new $this->Payroll_templates_model();
		$temp->setId( $id, true );
		$this->data['template'] = $temp->get();
		
		$this->load->model('Financial_items_settings_model');
		$items = new $this->Financial_items_settings_model();
		$items->setKey('type', true);
		$items->setValue('deduction', true);
		$items->setJoin('financial_items', 'financial_items.id = financial_items_settings.item_id');
		$items->setSelect("financial_items.*");
		$items->setSelect("financial_items_settings.*");
		$this->data['deductions'] = $items->populate();
		$this->load->view('payroll_templates_deductions', $this->data );
	}
	
	public function contributions( $id ) {
		$this->data['template_id'] = $id;
		$this->load->model(array('Financial_items_model','Payroll_templates_items_model'));
		
		if( $this->input->get("delete") != "" ) {
			$ptg = new $this->Payroll_templates_items_model();
			$ptg->setId( $this->input->get("delete"), true );
			$ptg->delete();
			redirect(uri_string(),"location");
		}
		
		$ptItem = new $this->Payroll_templates_items_model;
		$ptItem->setTempId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('item_id', 'Item', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$item_id = $this->input->post("item_id", true);
				$priority = $this->input->post("priority", true);
				
				$ptItem->setItemId( $item_id );
				$ptItem->setType("contribution");
				$ptItem->setPriority($priority);
				$ptItem->insert();
			}
		}
		
		$ptItem->setType("contribution", true);
		$ptItem->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$ptItem->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$ptItem->setOrder('payroll_templates_items.priority','ASC');
		$this->data['pt_items'] = $ptItem->populate();

		$temp = new $this->Payroll_templates_model();
		$temp->setId( $id, true );
		$this->data['template'] = $temp->get();
		
		$this->load->model('Financial_items_settings_model');
		$items = new $this->Financial_items_settings_model();
		$items->setKey('type', true);
		$items->setValue('contribution', true);
		$items->setJoin('financial_items', 'financial_items.id = financial_items_settings.item_id');
		$items->setSelect("financial_items.*");
		$items->setSelect("financial_items_settings.*");
		$this->data['contributions'] = $items->populate();
		$this->load->view('payroll_templates_contributions', $this->data );
	}
}
