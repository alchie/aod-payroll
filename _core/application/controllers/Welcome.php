<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		$this->data['sidebar_menu_main'] = "";
		$this->data['sidebar_menu_sub'] = "";
	}
	
	public function index()
	{
		$session_started = $this->session->userdata('session_started');
		if( $session_started ) {
			$this->load->view('welcome', $this->data);
		} else {
			if( count($this->input->post()) > 0 ) {
				$username = $this->input->post('username', TRUE);
				$password = $this->input->post('password', TRUE);
				
				$this->load->model('User_accounts_model');
				$user = new User_accounts_model();
				$user->setUsername( $username , TRUE );
				$user->setPassword( md5( $password ), TRUE );
				$user->setStatus( 1, TRUE );
				if( $user->nonEmpty() === TRUE ) {
					$result = $user->getResults();
					$this->session->set_userdata('session_started', TRUE);
					$this->session->set_userdata('logged_name', $result->name);
					$this->session->set_userdata('logged_uid', $result->uid);
					$this->session->set_userdata('logged_username', $result->username);
					$this->session->set_userdata('logged_account_type', $result->account_type);
					redirect("welcome", "location");
				} else {
					$this->data['error'] = "Unable to Login!";
				}
			}
			$this->load->view('login', $this->data );
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url("welcome"), "location");
	}
}
