<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financial_items extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_account_type') != 'admin' ) {
			redirect("welcome", "401");
		}
		
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "financial_items";
		$this->load->helper("gentelella");
		
		$this->load->model(array('Financial_items_model', 'Financial_items_settings_model'));
		
	}
	
	public function index()
	{
		$items = new $this->Financial_items_model();
		$this->data['items'] = $items->populate();
		$this->load->view('financial_items', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {
			
			$this->form_validation->set_rules('name', 'Item Name', 'trim|required');
			//$this->form_validation->set_rules('type', 'Item Type', 'trim|required');
			
			 if ($this->form_validation->run() == TRUE) {
			 
				$name = $this->input->post("name", true);
				//$type = $this->input->post("type", true);
				//$daily = $this->input->post("daily", true);
				$status = "1"; //$this->input->post("status", true);
				
				/* if( $daily == "") {
					$daily = 0;
				}
				*/
				if( $status == "") {
					$status = 0;
				}
				
				$temp = new $this->Financial_items_model();
				$temp->setName( $name );
				//$temp->setType( $type );
				//$temp->setDaily( $daily );
				$temp->setActive( $status );
				if( $temp->insert() ) {
					redirect("financial_items/update/" . $temp->getId(), "location");
				}
			}
		}
		
		$this->load->view('financial_items_add', $this->data);
	}
	
	public function update($id) {
		
		$item = new $this->Financial_items_model();
		$item->setId( $id, true );
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('name', 'Item Name', 'trim|required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$name = $this->input->post("name", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$item->setName( $name );
				$item->setActive( $status );
				$item->update();
			}
			
			
			foreach( $this->input->post("settings") as $key=>$setting ) {
				$set = new $this->Financial_items_settings_model;
				$set->setItemId( $id, true );
				$set->setKey( $key, true );
				$set->setValue( $setting );
				
				if( trim($setting) == '' ) {
					$set->delete();
				} else {
					if( $set->nonEmpty() === FALSE ) {
						$set->insert();
					} else {
						$set->setValue( $setting, false, true );
						$set->update();
					}
				}
			}
		}
		
		$set = new $this->Financial_items_settings_model;
		$set->setItemId($id, true);
		
		$fi_settings = array();
		foreach( $set->populate() as $fis ) {
			$fi_settings[$fis->key] = $fis->value;
		}
		$this->data['settings'] = $fi_settings;
		
		$this->data['item'] = $item->get();
		$this->load->view('financial_items_update', $this->data);
	}
	
	function delete( $id ) {
		$item = new $this->Financial_items_model();
		$item->setId( $id, true );
		$item->delete();
		redirect("financial_items", "location");
	}
}
