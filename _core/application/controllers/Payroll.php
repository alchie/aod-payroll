<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll extends CI_Controller {

	var $data = array();
	
	public function __construct() {
		parent::__construct();
				
		$this->data['sidebar_menu_main'] = "payroll_information";
		$this->data['sidebar_menu_sub'] = "payroll";
		$this->load->helper("gentelella");
		
		$this->load->model('Payroll_model');
		$this->load->model('Payroll_templates_model');
	}
	
	public function index()
	{
		$payrolls = new $this->Payroll_model();
		$payrolls->setJoin("payroll_templates", "payroll.template = payroll_templates.id");
		$payrolls->setSelect("payroll.*, payroll_templates.name as template_name");
		$this->data['payrolls'] = $payrolls->populate();
		$this->load->view('payroll', $this->data );
	}
	
	public function add() {
	
		$templates = new $this->Payroll_templates_model;
		$this->data['templates'] = $templates->populate();
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('month', 'Month', 'required');
			$this->form_validation->set_rules('year', 'Year', 'required');
			$this->form_validation->set_rules('desc', 'Description', 'required');
			$this->form_validation->set_rules('period_start', 'Period Start', 'required');
			$this->form_validation->set_rules('period_end', 'Period End', 'required');
			$this->form_validation->set_rules('template', 'Template', 'required');
			$this->form_validation->set_rules('days', 'Number of Days', 'required');
			 
			 if ($this->form_validation->run() == TRUE) {

				$month = $this->input->post("month", true);
				$year = $this->input->post("year", true);
				$desc = $this->input->post("desc", true);
				$period_start = $this->input->post("period_start", true);
				$period_end = $this->input->post("period_end", true);
				$tmplte = $this->input->post("template", true);
				$days = $this->input->post("days", true);
				$status = $this->input->post("status", true);
				
				
				if( $status == "") {
					$status = 0;
				}
				
				$payroll = new $this->Payroll_model();
				$payroll->setMonth( $month );
				$payroll->setYear( $year );
				$payroll->setDescription( $desc );
				$payroll->setPeriodStart( $period_start );
				$payroll->setPeriodEnd( $period_end );
				$payroll->setDays( $days );
				$payroll->setTemplate( $tmplte );
				$payroll->setActive( $status );
				if( $payroll->insert() ) {				
					redirect("payroll/update/" . $payroll->getId(), "location");
				}
			}
		}
		
		$this->load->view('payroll_add', $this->data);
	}
	
	public function update($id) {
		
		$templates = new $this->Payroll_templates_model;
		$this->data['templates'] = $templates->populate();
		
		$payroll = new $this->Payroll_model();
		$payroll->setId( $id, true );
		
		$payroll_data = $payroll->get();
		
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('month', 'Month', 'required');
			$this->form_validation->set_rules('year', 'Year', 'required');
			$this->form_validation->set_rules('desc', 'Description', 'required');
			$this->form_validation->set_rules('period_start', 'Period Start', 'required');
			$this->form_validation->set_rules('period_end', 'Period End', 'required');
			$this->form_validation->set_rules('template', 'Template', 'required');
			$this->form_validation->set_rules('days', 'Number of Days', 'required');
			 
			 if ($this->form_validation->run() == TRUE) {

				$month = $this->input->post("month", true);
				$year = $this->input->post("year", true);
				$desc = $this->input->post("desc", true);
				$period_start = $this->input->post("period_start", true);
				$period_end = $this->input->post("period_end", true);
				$tmplte = $this->input->post("template", true);
				$status = $this->input->post("status", true);
				$days = $this->input->post("days", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$payroll->setMonth( $month );
				$payroll->setYear( $year );
				$payroll->setDescription( $desc );
				$payroll->setPeriodStart( $period_start );
				$payroll->setPeriodEnd( $period_end );
				$payroll->setDays( $days );
				$payroll->setTemplate( $tmplte );
				$payroll->setActive( $status );
				$payroll->update();	
			}
		}
		
		$this->data['payroll'] = $payroll->get();
		$this->load->view('payroll_update', $this->data);
	}
	
	public function pr1nt($id)
	{
		$this->load->model(array(
			'Payroll_templates_model',
			'Payroll_templates_groups_model',
			'Payroll_templates_earnings_model',
			'Payroll_templates_deductions_model',
			'Payroll_templates_contributions_model',
			'Payroll_templates_items_model',
			'Employees_model'
		));
		
		$payroll = new $this->Payroll_model();
		$payroll->setId( $id, true );
		$this->data['payroll'] = $payroll->get();
		
		$payroll_template = new $this->Payroll_templates_model;
		$payroll_template->setId( $this->data['payroll']->template, true );
		$this->data['payroll_template'] = $payroll_template->get();
		
		$pt_groups = new $this->Payroll_templates_groups_model;
		$pt_groups->setTempId( $this->data['payroll_template']->id, true );
		$pt_groups->setSelect("payroll_templates_groups.*, employee_groups.name as group_name");
		$pt_groups->setJoin("employee_groups", "employee_groups.id = payroll_templates_groups.group_id");
		
		$pt_earnings = new $this->Payroll_templates_items_model;
		$pt_earnings->setTempId( $this->data['payroll_template']->id, true );
		$pt_earnings->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$pt_earnings->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$pt_earnings->setType('earning', true);
		$pt_earnings->setOrder('payroll_templates_items.priority','ASC');
		$this->data['earnings'] = $pt_earnings->populate();
		
		$pt_deductions = new $this->Payroll_templates_items_model;
		$pt_deductions->setTempId( $this->data['payroll_template']->id, true );
		$pt_deductions->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$pt_deductions->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$pt_deductions->setType('deduction', true);
		$pt_deductions->setOrder('payroll_templates_items.priority','ASC');
		$this->data['deductions'] = $pt_deductions->populate();
		
		$pt_contributions = new $this->Payroll_templates_items_model;
		$pt_contributions->setTempId( $this->data['payroll_template']->id, true );
		$pt_contributions->setSelect("payroll_templates_items.*, financial_items.name as item_name");
		$pt_contributions->setJoin("financial_items", "financial_items.id = payroll_templates_items.item_id");
		$pt_contributions->setType('contribution', true);
		$pt_contributions->setOrder('payroll_templates_items.priority','ASC');
		$this->data['contributions'] = $pt_contributions->populate();
		
		$employee_groups = $pt_groups->populate();
		foreach( $employee_groups as $key=>$empGroup ) {
			$emp = new $this->Employees_model;
			$emp->setGroup( $empGroup->group_id, true );
			$emp->setSelect("employees.*");
			foreach( $this->data['earnings'] as $eEarn) {
				$emp->setSelect("(SELECT `amount` FROM `employee_items` WHERE `item_id` = '{$eEarn->item_id}' AND `employee_id` = employees.id LIMIT 1) as item_" . $eEarn->item_id);
			}
			foreach( $this->data['deductions'] as $eDeduc) {
				$emp->setSelect("(SELECT `amount` FROM `employee_items` WHERE `item_id` = '{$eDeduc->item_id}' AND `employee_id` = employees.id LIMIT 1) as item_" . $eDeduc->item_id);
			}
			foreach( $this->data['contributions'] as $eContrib) {
				$emp->setSelect("(SELECT `amount` FROM `employee_items` WHERE `item_id` = '{$eContrib->item_id}' AND `employee_id` = employees.id LIMIT 1) as item_employee_" . $eContrib->item_id);
				$emp->setSelect("(SELECT `amount2` FROM `employee_items` WHERE `item_id` = '{$eContrib->item_id}' AND `employee_id` = employees.id LIMIT 1) as item_employer_" . $eContrib->item_id);
			}
			$emp->setLimit(0);
			$empGroup->members = $emp->populate();
			$employee_groups[$key] = $empGroup;
		}
		
		$this->data['employee_groups'] = $employee_groups;
		
		$this->load->view('payroll_print', $this->data );
	}
}
