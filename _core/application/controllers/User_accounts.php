<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_accounts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	var $data = array();
	
	public function __construct() {
		parent::__construct();
		
		if( $this->session->userdata('logged_account_type') != 'admin' ) {
			redirect("welcome", "401");
		}
		
		$this->data['sidebar_menu_main'] = "administration";
		$this->data['sidebar_menu_sub'] = "user_accounts";
		$this->load->helper("gentelella");
	}
	
	public function index()
	{
		$this->load->model('User_accounts_model');
		$users = new User_accounts_model();
		$this->data['users'] = $users->populate();
		$this->load->view('user_accounts', $this->data );
	}
	
	public function add() {
		if( count($this->input->post()) > 0 ) {

			$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|is_unique[user_accounts.username]');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
			$this->form_validation->set_rules('password2', 'Repeat Password', 'required|matches[password]');
			$this->form_validation->set_rules('name', 'Name', 'required');
			 
			 if ($this->form_validation->run() == TRUE) {
			 
				$username = $this->input->post("username", true);
				$password = $this->input->post("password", true);
				$password2 = $this->input->post("password2", true);
				$name = $this->input->post("name", true);
				$account_type = $this->input->post("account_type", true);
				$status = $this->input->post("status", true);
				
				if( $status == "") {
					$status = 0;
				}
				
				$this->load->model('User_accounts_model');
				$user = new $this->User_accounts_model();
				$user->setUsername( $username );
				$user->setPassword( md5( $password ) );
				$user->setName( $name );
				$user->setAccountType( $account_type );
				$user->setStatus( $status );
				$user->insert();
				
				redirect("user_accounts/update/" . $user->getUid(), "location");
			}
		}
		
		$this->load->view('user_accounts_add', $this->data);
	}
	
	public function update($username) {
		$this->load->model('User_accounts_model');
		$user = new $this->User_accounts_model();
		$user->setUsername( $username, true );
		
		$userdata = $user->get();
		
		if( count($this->input->post()) > 0 ) {
			$password = $this->input->post("password", true);
			$password2 = $this->input->post("password2", true);
			
			if($password != "") {
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('password2', 'Repeat Password', 'required|matches[password]');
			}
			$this->form_validation->set_rules('name', 'Name', 'required');
			
			if ($this->form_validation->run() == TRUE) {
			
				$name = $this->input->post("name", true);
				$account_type = $this->input->post("account_type", true);
				$status = $this->input->post("status", true);
				
				if($password != "") {
					$user->setPassword( md5( $password ), false, true );
				} 

				$user->setName( $name, false, true );

				if( $status == "") {
					$status = 0;
				}
				
				if( $userdata->username != $this->session->userdata('logged_username') ) {
					$user->setAccountType( $account_type, false, true );
					$user->setStatus( $status, false, true );
				}
				$user->update();
			}
		}
		
		$this->data['user'] = $user->get();
		$this->load->view('user_accounts_update', $this->data);
	}
}
