<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Payroll_templates_contributions_model Class
 *
 * Manipulates `payroll_templates_contributions` table on database

CREATE TABLE `payroll_templates_contributions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `temp_id` int(20) NOT NULL,
  `item_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `temp_id` (`temp_id`)
);

 * @package			Model
 * @project			Payroll System
 * @project_link	
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Payroll_templates_contributions_model extends CI_Model {

	protected $id;
	protected $temp_id;
	protected $item_id;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'payroll_templates_contributions.id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('payroll_templates_contributions', array('payroll_templates_contributions.id' => $this->id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('payroll_templates_contributions', $this->getData(), array('payroll_templates_contributions.id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('payroll_templates_contributions', array('payroll_templates_contributions.id' => $this->id ) );
		}
	}

	/**
	* Increment row by `id`
	**/

	public function incrementById() {
		if($this->id != '' && $this->countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}

	/**
	* Decrement row by `id`
	**/

	public function decrementById() {
		if($this->id != '' && $this->countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: temp_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `temp_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setTempId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->temp_id = ( ($value == '') && ($this->temp_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'payroll_templates_contributions.temp_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'temp_id';
		}
		return $this;
	}

	/** 
	* Get the value of `temp_id` variable
	* @access public
	* @return String;
	*/

	public function getTempId() {
		return $this->temp_id;
	}

	/**
	* Get row by `temp_id`
	* @param temp_id
	* @return QueryResult
	**/

	public function getByTempId() {
		if($this->temp_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('payroll_templates_contributions', array('payroll_templates_contributions.temp_id' => $this->temp_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `temp_id`
	**/

	public function updateByTempId() {
		if($this->temp_id != '') {
			$this->setExclude('temp_id');
			if( $this->getData() ) {
				return $this->db->update('payroll_templates_contributions', $this->getData(), array('payroll_templates_contributions.temp_id' => $this->temp_id ) );
			}
		}
	}


	/**
	* Delete row by `temp_id`
	**/

	public function deleteByTempId() {
		if($this->temp_id != '') {
			return $this->db->delete('payroll_templates_contributions', array('payroll_templates_contributions.temp_id' => $this->temp_id ) );
		}
	}

	/**
	* Increment row by `temp_id`
	**/

	public function incrementByTempId() {
		if($this->temp_id != '' && $this->countField != '') {
			$this->db->where('temp_id', $this->temp_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}

	/**
	* Decrement row by `temp_id`
	**/

	public function decrementByTempId() {
		if($this->temp_id != '' && $this->countField != '') {
			$this->db->where('temp_id', $this->temp_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: temp_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: item_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->item_id = ( ($value == '') && ($this->item_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'payroll_templates_contributions.item_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'item_id';
		}
		return $this;
	}

	/** 
	* Get the value of `item_id` variable
	* @access public
	* @return String;
	*/

	public function getItemId() {
		return $this->item_id;
	}

	/**
	* Get row by `item_id`
	* @param item_id
	* @return QueryResult
	**/

	public function getByItemId() {
		if($this->item_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('payroll_templates_contributions', array('payroll_templates_contributions.item_id' => $this->item_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `item_id`
	**/

	public function updateByItemId() {
		if($this->item_id != '') {
			$this->setExclude('item_id');
			if( $this->getData() ) {
				return $this->db->update('payroll_templates_contributions', $this->getData(), array('payroll_templates_contributions.item_id' => $this->item_id ) );
			}
		}
	}


	/**
	* Delete row by `item_id`
	**/

	public function deleteByItemId() {
		if($this->item_id != '') {
			return $this->db->delete('payroll_templates_contributions', array('payroll_templates_contributions.item_id' => $this->item_id ) );
		}
	}

	/**
	* Increment row by `item_id`
	**/

	public function incrementByItemId() {
		if($this->item_id != '' && $this->countField != '') {
			$this->db->where('item_id', $this->item_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}

	/**
	* Decrement row by `item_id`
	**/

	public function decrementByItemId() {
		if($this->item_id != '' && $this->countField != '') {
			$this->db->where('item_id', $this->item_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('payroll_templates_contributions');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: item_id
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('payroll_templates_contributions', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('payroll_templates_contributions');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('payroll_templates_contributions', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('payroll_templates_contributions', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('payroll_templates_contributions', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('payroll_templates_contributions');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('payroll_templates_contributions');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('payroll_templates_contributions');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'payroll_templates_contributions.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("id","temp_id","item_id");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->where, $this->filter, $this->where_or, $this->where_in, $this->where_in_or, $this->where_not_in, $this->where_not_in_or, $this->like, $this->like_or, $this->like_not, $this->like_not_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		$return = FALSE;
		if( $this->having ) {
			foreach( $this->having as $having ) {
				if ( is_array( $having ) ) {
					foreach( $having as $key => $value ) {
						$this->db->having( $key , $value );
					}
				} else {
					$this->db->having( $having );
				}
			}
			$return = TRUE;
		}
		if( $this->having_or ) {
			foreach( $this->having_or as $having_or ) {
				if ( is_array( $having_or ) ) {
					foreach( $having_or as $key => $value ) {
						$this->db->or_having( $key , $value );
					}
				} else {
					$this->db->or_having( $having_or );
				}
			}
			$return = TRUE;
		}
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge( $this->where, $this->filter, $this->where_or, $this->where_in, $this->where_in_or, $this->where_not_in, $this->where_not_in_or, $this->like, $this->like_or, $this->like_not, $this->like_not_or, $this->having, $this->having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'payroll_templates_contributions'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_array ( $select ) ) {
			$this->select = array_merge( $this->select, $select );
		} else {
			if( is_null( $index ) ) {
				$this->select[] = $select;
			} else {
				$this->select[$index] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->exclude = array_merge( $this->exclude, $exclude );
		} else {
			$this->exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('payroll_templates_contributions');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->setupJoin();
		$this->db->from('payroll_templates_contributions');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
}

/* End of file payroll_templates_contributions_model.php */
/* Location: ./application/models/payroll_templates_contributions_model.php */
