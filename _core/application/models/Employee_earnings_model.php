<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Employee_earnings_model Class
 *
 * Manipulates `employee_earnings` table on database

CREATE TABLE `employee_earnings` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `employee_id` int(20) NOT NULL,
  `item_id` int(20) NOT NULL,
  `amount` float NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

 * @package			Model
 * @project			Payroll System
 * @project_link	
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Employee_earnings_model extends CI_Model {

	protected $id;
	protected $employee_id;
	protected $item_id;
	protected $amount;
	protected $active;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->id = ( ($value == '') && ($this->id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'employee_earnings.id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'id';
		}
		return $this;
	}

	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

	public function getId() {
		return $this->id;
	}

	/**
	* Get row by `id`
	* @param id
	* @return QueryResult
	**/

	public function getById() {
		if($this->id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('employee_earnings', array('employee_earnings.id' => $this->id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `id`
	**/

	public function updateById() {
		if($this->id != '') {
			$this->setExclude('id');
			if( $this->getData() ) {
				return $this->db->update('employee_earnings', $this->getData(), array('employee_earnings.id' => $this->id ) );
			}
		}
	}


	/**
	* Delete row by `id`
	**/

	public function deleteById() {
		if($this->id != '') {
			return $this->db->delete('employee_earnings', array('employee_earnings.id' => $this->id ) );
		}
	}

	/**
	* Increment row by `id`
	**/

	public function incrementById() {
		if($this->id != '' && $this->countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('employee_earnings');
		}
	}

	/**
	* Decrement row by `id`
	**/

	public function decrementById() {
		if($this->id != '' && $this->countField != '') {
			$this->db->where('id', $this->id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('employee_earnings');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: employee_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `employee_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmployeeId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->employee_id = ( ($value == '') && ($this->employee_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'employee_earnings.employee_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'employee_id';
		}
		return $this;
	}

	/** 
	* Get the value of `employee_id` variable
	* @access public
	* @return String;
	*/

	public function getEmployeeId() {
		return $this->employee_id;
	}

	/**
	* Get row by `employee_id`
	* @param employee_id
	* @return QueryResult
	**/

	public function getByEmployeeId() {
		if($this->employee_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('employee_earnings', array('employee_earnings.employee_id' => $this->employee_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `employee_id`
	**/

	public function updateByEmployeeId() {
		if($this->employee_id != '') {
			$this->setExclude('employee_id');
			if( $this->getData() ) {
				return $this->db->update('employee_earnings', $this->getData(), array('employee_earnings.employee_id' => $this->employee_id ) );
			}
		}
	}


	/**
	* Delete row by `employee_id`
	**/

	public function deleteByEmployeeId() {
		if($this->employee_id != '') {
			return $this->db->delete('employee_earnings', array('employee_earnings.employee_id' => $this->employee_id ) );
		}
	}

	/**
	* Increment row by `employee_id`
	**/

	public function incrementByEmployeeId() {
		if($this->employee_id != '' && $this->countField != '') {
			$this->db->where('employee_id', $this->employee_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('employee_earnings');
		}
	}

	/**
	* Decrement row by `employee_id`
	**/

	public function decrementByEmployeeId() {
		if($this->employee_id != '' && $this->countField != '') {
			$this->db->where('employee_id', $this->employee_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('employee_earnings');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: employee_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: item_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `item_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setItemId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->item_id = ( ($value == '') && ($this->item_id != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'employee_earnings.item_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'item_id';
		}
		return $this;
	}

	/** 
	* Get the value of `item_id` variable
	* @access public
	* @return String;
	*/

	public function getItemId() {
		return $this->item_id;
	}

	/**
	* Get row by `item_id`
	* @param item_id
	* @return QueryResult
	**/

	public function getByItemId() {
		if($this->item_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('employee_earnings', array('employee_earnings.item_id' => $this->item_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `item_id`
	**/

	public function updateByItemId() {
		if($this->item_id != '') {
			$this->setExclude('item_id');
			if( $this->getData() ) {
				return $this->db->update('employee_earnings', $this->getData(), array('employee_earnings.item_id' => $this->item_id ) );
			}
		}
	}


	/**
	* Delete row by `item_id`
	**/

	public function deleteByItemId() {
		if($this->item_id != '') {
			return $this->db->delete('employee_earnings', array('employee_earnings.item_id' => $this->item_id ) );
		}
	}

	/**
	* Increment row by `item_id`
	**/

	public function incrementByItemId() {
		if($this->item_id != '' && $this->countField != '') {
			$this->db->where('item_id', $this->item_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('employee_earnings');
		}
	}

	/**
	* Decrement row by `item_id`
	**/

	public function decrementByItemId() {
		if($this->item_id != '' && $this->countField != '') {
			$this->db->where('item_id', $this->item_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('employee_earnings');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: item_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: amount
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `amount` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->amount = ( ($value == '') && ($this->amount != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'employee_earnings.amount';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'amount';
		}
		return $this;
	}

	/** 
	* Get the value of `amount` variable
	* @access public
	* @return String;
	*/

	public function getAmount() {
		return $this->amount;
	}

	/**
	* Get row by `amount`
	* @param amount
	* @return QueryResult
	**/

	public function getByAmount() {
		if($this->amount != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('employee_earnings', array('employee_earnings.amount' => $this->amount), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `amount`
	**/

	public function updateByAmount() {
		if($this->amount != '') {
			$this->setExclude('amount');
			if( $this->getData() ) {
				return $this->db->update('employee_earnings', $this->getData(), array('employee_earnings.amount' => $this->amount ) );
			}
		}
	}


	/**
	* Delete row by `amount`
	**/

	public function deleteByAmount() {
		if($this->amount != '') {
			return $this->db->delete('employee_earnings', array('employee_earnings.amount' => $this->amount ) );
		}
	}

	/**
	* Increment row by `amount`
	**/

	public function incrementByAmount() {
		if($this->amount != '' && $this->countField != '') {
			$this->db->where('amount', $this->amount);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('employee_earnings');
		}
	}

	/**
	* Decrement row by `amount`
	**/

	public function decrementByAmount() {
		if($this->amount != '' && $this->countField != '') {
			$this->db->where('amount', $this->amount);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('employee_earnings');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: amount
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->active = ( ($value == '') && ($this->active != '') ) ? '' : $value;
		if( $setWhere ) {
			$key = 'employee_earnings.active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}
			
			switch( $underCondition ) {
				case 'where_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereOr($key, $value, $priority);
				break;
				case 'where_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereIn($key, $value, $priority);
				break;
				case 'where_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereInOr($key, $value, $priority);
				break;
				case 'where_not_in':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotIn($key, $value, $priority);
				break;
				case 'where_not_in_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setWhereNotInOr($key, $value, $priority);
				break;
				case 'like':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLike($key, $value, $priority);
				break;
				case 'like_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeOr($key, $value, $priority);
				break;
				case 'like_not':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNot($key, $value, $priority);
				break;
				case 'like_not_or':
					if( ! is_array( $value ) ) {
						$value = explode(',', $value );
					}
					$this->setLikeNotOr($key, $value, $priority);
				break;
				default:
					$this->setWhere($key, $value, $priority);
				break;
			}
		}
		if( $set_data_field ) {
			$this->dataFields[] = 'active';
		}
		return $this;
	}

	/** 
	* Get the value of `active` variable
	* @access public
	* @return String;
	*/

	public function getActive() {
		return $this->active;
	}

	/**
	* Get row by `active`
	* @param active
	* @return QueryResult
	**/

	public function getByActive() {
		if($this->active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('employee_earnings', array('employee_earnings.active' => $this->active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `active`
	**/

	public function updateByActive() {
		if($this->active != '') {
			$this->setExclude('active');
			if( $this->getData() ) {
				return $this->db->update('employee_earnings', $this->getData(), array('employee_earnings.active' => $this->active ) );
			}
		}
	}


	/**
	* Delete row by `active`
	**/

	public function deleteByActive() {
		if($this->active != '') {
			return $this->db->delete('employee_earnings', array('employee_earnings.active' => $this->active ) );
		}
	}

	/**
	* Increment row by `active`
	**/

	public function incrementByActive() {
		if($this->active != '' && $this->countField != '') {
			$this->db->where('active', $this->active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('employee_earnings');
		}
	}

	/**
	* Decrement row by `active`
	**/

	public function decrementByActive() {
		if($this->active != '' && $this->countField != '') {
			$this->db->where('active', $this->active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('employee_earnings');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: active
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('employee_earnings', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('employee_earnings');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('employee_earnings', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('employee_earnings', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('employee_earnings', $this->getData() ) === TRUE ) {
				$this->id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('employee_earnings');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('employee_earnings');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('employee_earnings');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'employee_earnings.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("id","employee_id","item_id","amount","active");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge( $this->where, $this->filter, $this->where_or, $this->where_in, $this->where_in_or, $this->where_not_in, $this->where_not_in_or, $this->like, $this->like_or, $this->like_not, $this->like_not_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		$return = FALSE;
		if( $this->having ) {
			foreach( $this->having as $having ) {
				if ( is_array( $having ) ) {
					foreach( $having as $key => $value ) {
						$this->db->having( $key , $value );
					}
				} else {
					$this->db->having( $having );
				}
			}
			$return = TRUE;
		}
		if( $this->having_or ) {
			foreach( $this->having_or as $having_or ) {
				if ( is_array( $having_or ) ) {
					foreach( $having_or as $key => $value ) {
						$this->db->or_having( $key , $value );
					}
				} else {
					$this->db->or_having( $having_or );
				}
			}
			$return = TRUE;
		}
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge( $this->where, $this->filter, $this->where_or, $this->where_in, $this->where_in_or, $this->where_not_in, $this->where_not_in_or, $this->like, $this->like_or, $this->like_not, $this->like_not_or, $this->having, $this->having_or );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'employee_earnings'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_array ( $select ) ) {
			$this->select = array_merge( $this->select, $select );
		} else {
			if( is_null( $index ) ) {
				$this->select[] = $select;
			} else {
				$this->select[$index] = $select;
			}
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		if( is_array ( $exclude ) ) {
			$this->exclude = array_merge( $this->exclude, $exclude );
		} else {
			$this->exclude[] = $exclude;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('employee_earnings');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->setupJoin();
		$this->db->from('employee_earnings');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
}

/* End of file employee_earnings_model.php */
/* Location: ./application/models/employee_earnings_model.php */
